const express = require('express');
const router = express.Router();

const { pushNotification } = require('../controllers/notification')

router.post('/push', pushNotification);

module.exports = router;