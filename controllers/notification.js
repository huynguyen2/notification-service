const admin = require('firebase-admin');
const serviceAccount = require('../assets/trend-solar-90027-firebase-adminsdk-yn9ig-d83ec550d9.json');
const NotificationTokens = require('../models/NotificationTokens');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

exports.pushNotification = async (req, res) => {
  const { token, title, body, data, click_action } = req.body;

  const notificationToken = await NotificationTokens.findOne({ where: { token } });
  if (!notificationToken) {
    return res.status(400).json({
      result: 'fail',
      message: 'Token not found'
    });
  }
  
  // Notification content
  const message = {
    // topic: 'test',
    notification: {
      title,
      body
    },
    data,
    android: {
      notification: {
        click_action
      }
    },
    token
  }

  try {
    // Response is a message ID string.
    const messageID = await admin.messaging().send(message);
    console.log('Successfully sent message:', messageID);

    res.status(200).json({
      result: 'success',
      messageID
    });

  } catch (error) {
    console.log('Error sending message:', error);

    res.status(400).json({
      result: 'success',
      message: error
    });
  }

}