const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const notificationRouter = require('./routes/notification');

const app = express();

// Control original resource sharing.
app.use(cors());

// Middlewares
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

// Router
app.use('/notification', notificationRouter);

const port = process.env.PORT || 8080;
app.listen(port, console.log(`Server listening on port ${port}`));