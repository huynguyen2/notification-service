const { DataTypes } = require("sequelize");
const sequelize = require('../utils/db');

const NotificationTokensSchema = sequelize.define('notification_tokens', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  token: {
    type: DataTypes.STRING(255),
    allowNull: true
  },
  user_id: {
    type: DataTypes.STRING(45),
    allowNull: true
  },
  created_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
  }
}, {
  sequelize,
  tableName: 'notification_tokens',
  timestamps: false
});

module.exports = NotificationTokensSchema;